<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header();
?>
<link rel="stylesheet" href="style.css"  />
	<main id="primary" class="site-main">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<img  class="image" src="<?php the_field ('image');?>">
<img class="image" src="<?php the_field ('image2');?>">
<img class="image" src="<?php the_field ('image3');?>">
<img class="image" src="<?php the_field ('image4');?>">
<img class="image" src="<?php the_field ('image5');?>">	
			
<?php endwhile; else: ?>
<?php endif; ?>			

<?php
			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', '_s' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', '_s' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		//endwhile; // End of the loop.
		?>

	
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
